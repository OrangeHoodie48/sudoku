﻿using System;
using Sudoku.SmartSudoku;
using Sudoku.SmartSudoku.SolvingAlgorithms;
using Sudoku.SmartSudoku.NumberReducer;
namespace Sudoku
{
    class Program
    {
        static void Main(string[] args)
        {



            string hardestPuzzle =
                                  "800000000" +
                                  "003600000" +
                                  "070090200" +
                                  "050007000" +
                                  "000045700" +
                                  "000100030" +
                                  "001000068" +
                                  "008500010" +
                                  "090000400";

            string testPuzzleMedium = "609080310" +
                                "030060504" +
                                "004000900" +
                                "000700859" +
                                "000050000" +
                                "856004000" +
                                "008000600" +
                                "207010090" +
                                "095040208";

            string testPuzzleHard = "300502400" +
                                    "000070000" +
                                    "200080061" +
                                    "409000000" +
                                    "060937020" +
                                    "000000709" +
                                    "520090008" +
                                    "000020000" +
                                    "004601003";

            NumberReducer reducer = new NumberReducer(); 
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine(SudokuGenerator.FormatBoard(reducer.GeneratePuzzle(1)));
            }

            Console.ReadLine();


        }

        
    }
}
